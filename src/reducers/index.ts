import {Action} from "redux";
import {District, initialState, Probs, State} from "../models";
import {
    CHANGE_PROB,
    ChangeProb, EDIT_PROBS_TEXT, EditProbsText,
    FETCH_PRICES_FAILURE,
    FETCH_PRICES_SUCCESS,
    FetchPricesFailure,
    FetchPricesSuccess,
    LOAD_PROBS,
    LoadProbs,
    PRINT_PROBS,
    PrintProbs
} from "../actions";
import {getPartyByName, Party} from "../models/Party";
import {MultiEvent} from "../models/MultiEvent";

function reduce (state: State = initialState, action: Action<string>) : State {
    switch (action.type) {
        case FETCH_PRICES_SUCCESS: {
            let districts: District[] = districtsFromJSON((<FetchPricesSuccess> action).data);
            let multiEventInit: {[event: string]: {[outcome: string]: number}} = {};
            districts.forEach (
                district => {
                    multiEventInit[district.name] = district.candidatePrices;
                }
            );
            console.log(multiEventInit);
            return Object.assign({}, state, {
                districts,
                multiEvent: MultiEvent.init(multiEventInit)
            });
        }
        case FETCH_PRICES_FAILURE: {
            console.error('Failed to load prices: ' + (<FetchPricesFailure> action).message);
            return state;
        }
        case CHANGE_PROB: {
            let multiEvent = state.multiEvent;
            if (multiEvent) {
                let actionData: ChangeProb = <ChangeProb> action;
                return Object.assign({}, state, {
                    multiEvent: MultiEvent.changeProb (
                        multiEvent,
                        actionData.district,
                        actionData.party,
                        actionData.prob
                    )
                });
            }
            return state;
        }
        case LOAD_PROBS: {
            let multiEvent = state.multiEvent;
            if (multiEvent) {
                let actionData: LoadProbs = <LoadProbs> action;
                let probs: Probs = JSON.parse(actionData.probsJSON);
                return Object.assign({}, state, {
                    multiEvent: MultiEvent.changeProbs(multiEvent, probs)
                });
            }
            return state;
        }
        case PRINT_PROBS: {
            console.log(state);
            let multiEvent = state.multiEvent;
            if (multiEvent) {
                let actionData: PrintProbs = <PrintProbs> action;
                return Object.assign({}, state, {
                    stateJSON: JSON.stringify(actionData.probs)
                });
            }
            return state;
        }
        case EDIT_PROBS_TEXT: {
            let actionData: EditProbsText = <EditProbsText> action;
            return Object.assign({}, state, {
                stateJSON: actionData.text
            });
        }
    }
    return state;
}

function districtsFromJSON (data: any) : District[] {
    let statesData: any[] = (<any[]> data.events).filter (
        e => (e.name.includes('Australian Federal Election -'))
    );
    return statesData.flatMap (
        stateData => {
            return stateData.marketList.map (
                (districtData: any) => {
                    let name: string = districtData.name.split('(')[0].trim();
                    let candidatePrices: {[party: string]: number} = {};
                    for (let candidateData of districtData.selections) {
                        let party: Party | undefined = getPartyByName(candidateData.name);
                        if (party) {
                            candidatePrices[party.tag] = candidateData.price.winPrice;
                        }
                    }
                    return {name, candidatePrices};
                }
            );
        }
    );
}

export default reduce;
