import {Action, Dispatch} from "redux";
import {Probs, State} from "../models";

export const CHANGE_PROB = 'CHANGE_PROB';
export interface ChangeProb extends Action<string> {
    district: string,
    party: string,
    prob: number
}
export function changeProb (district: string, party: string, prob: number) : ChangeProb {
    return {
        type: CHANGE_PROB,
        district,
        party,
        prob
    };
}

export const LOAD_PROBS = 'LOAD_PROBS';
export interface LoadProbs extends Action<string> {
    probsJSON: string
}
export function loadProbs (probsJSON: string) : LoadProbs {
    return {
        type: LOAD_PROBS,
        probsJSON
    };
}

export const PRINT_PROBS = 'PRINT_PROBS';
export interface PrintProbs extends Action<string> {
    probs: Probs
}
export function printProbs (probs: Probs) : PrintProbs {
    return {
        type: PRINT_PROBS,
        probs
    };
}

export const EDIT_PROBS_TEXT = 'EDIT_PROBS_TEXT';
export interface EditProbsText extends Action<string> {
    text: string
}
export function editProbsText (text: string) : EditProbsText {
    return {
        type: EDIT_PROBS_TEXT,
        text
    };
}

export const FETCH_PRICES_SUCCESS = 'FETCH_PRICES_SUCCESS';
export interface FetchPricesSuccess extends Action<string> {
    data: any
}
export function fetchPricesSuccess (data: any) : FetchPricesSuccess {
    return {
        type: FETCH_PRICES_SUCCESS,
        data
    };
}

export const FETCH_PRICES_FAILURE = 'FETCH_PRICES_FAILURE';
export interface FetchPricesFailure extends Action<string> {
    message: string
}
export function fetchPricesFailure (message: string) : FetchPricesFailure {
    return {
        type: FETCH_PRICES_FAILURE,
        message
    };
}

export const FETCH_PRICES = 'FETCH_PRICES';
export interface FetchPrices extends Action<string> {
    (dispatch: Dispatch, state: State) : void
}
export function fetchPrices () : FetchPrices {
    function actionFn (dispatch: Dispatch, state: State) : void {
        fetch('prices.json').then (
            response => response.json(),
            error => dispatch(fetchPricesFailure(error.toString()))
        ) .then (
            data => dispatch(fetchPricesSuccess(data))
        );
    }
    actionFn.type = FETCH_PRICES;
    return (<FetchPrices> actionFn);
}