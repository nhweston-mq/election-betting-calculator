import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';
import {applyMiddleware, createStore} from "redux";
import reducers from './reducers';
import thunk from 'redux-thunk';
import {Provider} from "react-redux";
import {initialState} from "./models";

const store = createStore(
    reducers,
    initialState,
    applyMiddleware (
        thunk
    )
);

ReactDOM.render (
    <Provider store={store}>
        <App />
    </Provider>,
    document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
serviceWorker.unregister();
