import {connect} from "react-redux";
import * as React from "react";
import {Component, ReactNode} from "react";
import {District, State} from "../models";
import TableRow from "./TableRow";
import {Dispatch} from "redux";
import {fetchPrices} from "../actions";
import {parties, Party} from "../models/Party";
import {MultiEvent} from "../models/MultiEvent";

interface Props {
    districts?: District[]
    multiEvent?: MultiEvent
    budget: number
    fetchFn: () => void
}

const mapStateToProps = (state: State) => {
    return {
        districts: state.districts,
        multiEvent: state.multiEvent,
        budget: state.budget
    };
};

const mapDispatchToProps = (dispatch: Dispatch) => {
    return {
        fetchFn: () => dispatch(fetchPrices())
    };
};

function renderPartyHeaders () {
    return parties.map (
        (party, index) => <th key={index}>{party.tag}</th>
    )
}

class Table extends Component<Props, {}> {

    render () : ReactNode {
        let districtsOpt = this.props.districts;
        let multiEventOpt = this.props.multiEvent;
        if (districtsOpt && multiEventOpt) {
            let districts: District[] = districtsOpt;
            let multiEvent: MultiEvent = multiEventOpt;
            let bets: {[event: string]: {[outcome: string]: number}} = multiEvent.getBets(this.props.budget);
            return (
                <table style={{float: 'left'}}><tbody>
                <tr>
                    <th>&nbsp;</th>
                    <th colSpan={parties.length + 1}>Perceived Odds</th>
                    <th colSpan={parties.length}>Prices</th>
                    <th colSpan={parties.length}>Amounts</th>
                </tr>
                <tr><th>District</th>
                    {renderPartyHeaders()}
                    <th>Other</th>
                    {renderPartyHeaders()}
                    {renderPartyHeaders()}
                </tr>{
                    districts.map (
                        (district, index) => (
                            <TableRow
                                key={index}
                                district={district}
                                event={multiEvent.events[district.name]}
                                bets={bets[district.name]}
                            />
                        )
                    )
                }
                </tbody></table>
            );
        }
        return <span>Loading...</span>;
    }

    componentDidMount () : void {
        this.props.fetchFn();
    }

}

export default connect (mapStateToProps, mapDispatchToProps) (Table)
