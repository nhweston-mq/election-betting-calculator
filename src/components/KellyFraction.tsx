import {Component, ReactNode} from "react";
import * as React from "react";
import {State} from "../models";
import {connect} from "react-redux";

interface Props {
    display: boolean
    frac: number
    bet: number
}

const mapStateToProps = (state: State, props: Props) => {
    return props;
};

const colors: string[] = [
    '#FCFFE5',
    '#F4FFCC',
    '#E8FFB2',
    '#D6FF99',
    '#BFFF7F',
    '#A3FF65',
    '#82FF4C',
    '#5BFF32',
    '#30FF19',
    '#00FF00',
    '#00FF00'
];

class KellyFraction extends Component<Props, {}> {

    constructor (props: Props) {
        super(props);
    }

    render () : ReactNode {
        console.log(this.props.frac);
        if (!this.props.display) {
            return (
                <td style={{
                    textAlign: 'center',
                    backgroundColor: '#EEEEEE',
                    minWidth: '40px'
                }}>&nbsp;</td>
            );
        }
        let frac = this.props.frac;
        if (frac <= 0) {
            return <td style={{textAlign: 'center', minWidth: '40px'}}>-</td>
        }
        return <td style={{
            textAlign: 'right',
            backgroundColor: colors[Math.trunc(frac * 10)],
            minWidth: '40px'
        }}>{'$' + this.props.bet.toFixed(2)}</td>
    }

}

export default connect (mapStateToProps) (KellyFraction);
