import {District} from "../models";
import {Event} from "../models/Event"
import * as React from "react";
import {Component, ReactNode} from "react";
import {connect} from "react-redux";
import Odds from "./Odds";
import Price from "./Price";
import OddsOther from "./OddsOther";
import KellyFraction from "./KellyFraction";
import {parties, Party} from "../models/Party";

interface Props {
    district: District
    event: Event
    bets: {[outcome: string]: number}
}

class TableRow extends Component<Props, {}> {

    render () : ReactNode {
        let candidates: {[party: string]: {
            prob?: number
            price?: number
            frac?: number
            bet?: number
        }} = {};
        let outcomes = this.props.event ? this.props.event.outcomes : [];
        outcomes.forEach (
            party => {
                candidates[party.label] = {
                    prob: party.prob,
                    price: party.price,
                    frac: this.props.event.fractions[party.label],
                    bet: this.props.bets[party.label]
                }
            }
        );
        let probsSum: number = 1;
        return (
            <tr><td>{this.props.district.name}</td>{
                parties.map (
                    (party, index) => {
                        if (candidates[party.tag]) {
                            let probOpt = candidates[party.tag].prob;
                            let prob = probOpt ? probOpt : 0;
                            probsSum = probsSum - prob;
                            return (
                                <Odds
                                    key={index}
                                    district={this.props.district}
                                    isPresent={true}
                                    party={party}
                                    prob={prob}
                                />
                            );
                        }
                        else return (
                            <Odds
                                key={index}
                                district={this.props.district}
                                isPresent={false}
                                party={party}
                                prob={0}
                            />
                        );
                    }
                )
            }<OddsOther key={0} value={probsSum*100}/>{
                parties.map (
                    (party, index) => {
                        if (candidates[party.tag]) {
                            let priceOpt = candidates[party.tag].price;
                            let price = priceOpt ? priceOpt : 1;
                            return <Price key={index} isPresent={true} price={price}/>;
                        }
                        else return <Price key={index} isPresent={false} price={1}/>
                    }
                )
            }{
                parties.map (
                    (party, index) => {
                        if (candidates[party.tag]) {
                            let fracOpt = candidates[party.tag].frac;
                            let frac = fracOpt ? fracOpt : 0;
                            let betOpt = candidates[party.tag].bet;
                            let bet = betOpt ? betOpt : 0;
                            return <KellyFraction
                                key={index}
                                display={true}
                                frac={frac}
                                bet={bet}
                            />
                        }
                        else return (
                            <KellyFraction
                                key={index}
                                display={false}
                                frac={0}
                                bet={0}
                            />
                        )
                    }
                )
            }</tr>
        )
    }

}

export default connect (s => {return {}}) (TableRow)
