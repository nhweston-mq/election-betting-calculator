import * as React from "react";
import {Component, ReactNode} from "react";

interface Props {
    isPresent: boolean
    price: number
}

const divPrice = (price: number) => (
    <div style={{
        float: 'right',
        display: 'table-cell',
        textAlign: 'right'
    }}>{price.toFixed(2)}</div>
);

export default class Price extends Component<Props, {}> {

    constructor (props: Props) {
        super(props);
    }

    render () : ReactNode {
        if (this.props.isPresent) {
            return (
                <td style={{width: 'auto'}}>
                    {divPrice(this.props.price)}
                </td>
            );
        }
        return <td style={{textAlign: 'center'}}>-</td>
    }

}
