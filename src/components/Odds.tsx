import {District, State} from "../models";
import {Dispatch} from "redux";
import {connect} from "react-redux";
import * as React from "react";
import {ChangeEventHandler, Component, ReactNode} from "react";
import {Party} from "../models/Party";
import {changeProb} from "../actions";

interface Props {
    isPresent: boolean
    district: District
    party: Party
    dispatch: Dispatch
    prob: number
}

const mapDispatchToProps = (dispatch: Dispatch) => {
    return {dispatch};
};

class Odds extends Component<Props, {}> {

    onChange: ChangeEventHandler<HTMLInputElement> = e => {
        this.props.dispatch (
            changeProb (
                this.props.district.name,
                this.props.party.tag,
                e.currentTarget.value ? parseFloat(e.currentTarget.value) / 100 : 0
            )
        );
    };

    constructor (props: Props) {
        super(props);
    }

    render () : ReactNode {
        if (this.props.isPresent) {
            return (
                <td style={{backgroundColor: this.props.party.color}}><input
                    type='number'
                    value={(this.props.prob * 100).toFixed(0)}
                    onChange={this.onChange}
                    style={{
                        maxWidth: '50px',
                        textAlign: 'right'
                    }}/>
                </td>
            );
        }
        return <td style={{textAlign: 'center'}}>-</td>;
    }

}

export default connect (() => {return {}}, mapDispatchToProps) (Odds);
