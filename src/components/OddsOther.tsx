import {Component, ReactNode} from "react";
import * as React from "react";
import {connect} from "react-redux";
import {State} from "../models";

interface Props {
    value: number
}

export default class OddsOther extends Component<Props, {}> {

    constructor (props: Props) {
        super(props);
    }

    render () : ReactNode {
        return <td>{this.props.value.toFixed(0)}</td>;
    }

}