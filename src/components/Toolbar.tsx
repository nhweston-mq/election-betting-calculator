import * as React from "react";
import {ChangeEventHandler, Component, MouseEventHandler, ReactNode} from "react";
import {getProbs, Probs, State} from "../models";
import {connect} from "react-redux";
import {Dispatch} from "redux";
import {editProbsText, loadProbs, printProbs} from "../actions";
import {MultiEvent} from "../models/MultiEvent";

interface Props {
    multiEvent?: MultiEvent
    stateJSON: string
    loadFn: (json: string) => void
    printFn: (data: Probs) => void
    editFn: (text: string) => void
}

const mapStateToProps = (state: State) => {
    let {multiEvent, stateJSON} = state;
    return {
        multiEvent,
        stateJSON: stateJSON ? stateJSON : '',
    };
};

const mapDispatchToProps = (dispatch: Dispatch) => {
    return {
        loadFn: (json: string) => dispatch(loadProbs(json)),
        printFn: (data: Probs) => dispatch(printProbs(data)),
        editFn: (text: string) => dispatch(editProbsText(text))
    }
};

class Toolbar extends Component<Props> {

    text: string = this.props.stateJSON;

    onClickLoad: MouseEventHandler<HTMLButtonElement> = e => {
        this.props.loadFn(this.text);
    };

    onClickPrint: MouseEventHandler<HTMLButtonElement> = e => {
        let multiEvent = this.props.multiEvent;
        if (multiEvent) {
            let probs = getProbs(multiEvent);
            this.props.printFn(probs);
        }
    };

    onEdit: ChangeEventHandler<HTMLTextAreaElement> = e => {
        this.props.editFn(e.currentTarget.value);
    };

    render () : ReactNode {
        this.text = this.props.stateJSON;
        return (
            <div style={{width: 'auto', float: 'left', padding: '16px', verticalAlign: 'top'}}>
                <textarea
                    style={{fontSize: '10px', width: '100%', height: '600px', resize: 'none'}}
                    value={this.props.stateJSON}
                    onChange={this.onEdit}
                />
                <button onClick={this.onClickLoad}>Load</button>&nbsp;
                <button onClick={this.onClickPrint}>Print</button>&nbsp;
                <button onClick={e => open (
                    'https://www.sportsbet.com.au/apigw/sportsbook-sports/Sportsbook/Sports/Competitions/5787' +
                    '?displayType=default&eventFilter=none&numMarkets=10000'
                )}>Prices</button>
            </div>
        );
    }

}

export default connect (mapStateToProps, mapDispatchToProps) (Toolbar);
