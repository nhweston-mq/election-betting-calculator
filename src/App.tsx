import React, {Component} from 'react';
import './App.css';
import Table from "./components/Table";
import Toolbar from "./components/Toolbar";

class App extends Component {
    render() {
        return (
            <div style={{width: '90%', padding: '16px', margin: 'auto'}}>
                <Table key={0}/>
                <Toolbar/>
            </div>
        );
    }
}

export default App;
