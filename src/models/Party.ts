export interface Party {
    tag: string
    color: string
}

function create (tag: string, color: string) : Party {
    return {tag, color};
}

export const L_N: Party = create ('L/N', '#7FC1FF');
export const ALP: Party = create ('ALP', '#FF7F85');
export const GRN: Party = create ('GRN', '#C1FF7F');
export const ON:  Party = create ('ON',  '#FFFF7F');
export const CA:  Party = create ('CA',  '#FFBF7F');
export const KAP: Party = create ('KAP', '#FF9F7F');
export const SFF: Party = create ('SFF', '#FF7FDF');
export const IND: Party = create ('IND', '#BFBFBF');

export const parties: Party[] = [L_N, ALP, GRN, ON, CA, KAP, SFF, IND];

export function getPartyByName (name: String) : Party | undefined {
    switch (name) {
        case 'Coalition': return L_N;
        case 'Tony Abbott (Coalition)': return L_N;
        case 'Dean Harris (Labor)': return ALP;
        case 'Labor': return ALP;
        case 'Green': return GRN;
        case 'Greens': return GRN;
        case 'One Nation': return ON;
        case 'Centre Alliance': return CA;
        case 'Katter\'s Australian Party': return KAP;
        case 'Shooters, Fishers and Farmers': return SFF;
        case 'Independent': return IND;
        case 'Zali Steggall (Independent)': return IND;
        case 'Julia Banks (Independent)': return IND;
    }
    return undefined;
}
