import {MultiEvent} from "./MultiEvent";

export interface State {
    stateJSON?: string
    districts?: District[]
    multiEvent?: MultiEvent
    budget: number
}

export interface District {
    name: string
    candidatePrices: {[party: string]: number}
}

export type Probs = {[district: string]: {[party: string]: number}}

export function getProbs (multiEvent: MultiEvent) : Probs {
    let result: Probs = {};
    let events = multiEvent.events;
    for (let district in events) {
        let candidateProbs: {[party: string]: number} = {};
        let outcomes = events[district].outcomes;
        for (let party of outcomes) {
            candidateProbs[party.label] = party.prob;
        }
        result[district] = candidateProbs;
    }
    return result;
}

export const initialState: State = {
    budget: 100
};
