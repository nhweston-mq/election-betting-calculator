import {Event} from "./Event";
import {Outcome} from "./Outcome";

export interface MultiEvent {
    events: {[label: string]: Event}
    sumFractions: number
    proportions: {[event: string]: {[outcome: string]: number}}
    getBets: (budget: number) => {[event: string]: {[outcome: string]: number}}
}

export const MultiEvent = {
    create,
    init,
    changeProb,
    changeProbs
};

function getBets (proportions: {[event: string]: {[outcome: string]: number}}, budget: number) {
    let result: {[event: string]: {[outcome: string]: number}} = {};
    for (let event in proportions) {
        let outcomes: {[outcome: string]: number} = proportions[event];
        let inner: {[outcome: string]: number} = {};
        for (let outcome in outcomes) {
            inner[outcome] = outcomes[outcome] * budget;
        }
        result[event] = inner;
    }
    return result;
}

function create (events: {[label: string]: Event}) : MultiEvent {
    let sumFractions = 0;
    for (let label in events) {
        sumFractions += events[label].sumFractions;
    }
    let proportions: {[event: string]: {[outcome: string]: number}} = {};
    for (let label in events) {
        proportions[label] = events[label].getProportions(sumFractions);
    }
    return {
        events, sumFractions, proportions,
        getBets: budget => getBets(proportions, budget)
    }
}

function init (prices: {[event: string]: {[outcome: string]: number}}) : MultiEvent {
    let events: {[label: string]: Event} = {};
    for (let eventLabel in prices) {
        let outcomePrices: {[outcome: string]: number} = prices[eventLabel];
        let outcomes: Outcome[] = [];
        for (let outcomeLabel in outcomePrices) {
            outcomes.push(Outcome.create(outcomeLabel, outcomePrices[outcomeLabel], 0));
        }
        events[eventLabel] = Event.create(outcomes);
    }
    return create(events);
}

function changeProb (
    source: MultiEvent,
    eventLabel: string,
    outcomeLabel: string,
    prob: number
) : MultiEvent {
    let events: {[label: string]: Event} = Object.assign({}, source.events);
    events[eventLabel] = Event.changeProb(events[eventLabel], outcomeLabel, prob);
    return create(events);
}

function changeProbs (
    source: MultiEvent,
    probs: {[event: string]: {[outcome: string]: number}}
) : MultiEvent {
    let events: {[label: string]: Event} = {};
    for (let eventLabel in source.events) {
        let outcomeProbs: {[outcome: string]: number} | undefined = probs[eventLabel];
        if (outcomeProbs) {
            events[eventLabel] = Event.changeProbs(source.events[eventLabel], outcomeProbs);
        }
        else {
            events[eventLabel] = source.events[eventLabel];
        }
    }
    return create(events);
}
