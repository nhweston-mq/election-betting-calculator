export interface Outcome {
    label: string
    price: number
    prob: number
    expected: number
}

export const Outcome = {
    create,
    changeProb
};

function create (label: string, price: number, prob: number) : Outcome {
    return {
        label, price, prob,
        expected: price * prob
    };
}

function changeProb (source: Outcome, prob: number) : Outcome {
    return create (
        source.label,
        source.price,
        prob
    );
}
