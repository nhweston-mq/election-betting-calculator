import {Outcome} from "./Outcome";

export interface Event {
    outcomes: Outcome[]
    selection: Outcome[]
    reserveRate: number
    fractions: {[outcome: string]: number}
    sumFractions: number
    getProportions: (total: number) => {[outcome: string]: number}
}

export const Event = {
    create,
    changeProb,
    changeProbs
};

function getReserveRate (outcomes: Outcome[]) : number {
    const div = outcomes.reduce (
        (div, o) => {
            return {
                num: div.num - o.prob,
                den: div.den - 1/o.price
            }
        },
        {num: 1, den: 1}
    );
    return div.num / div.den;
}

function select (selected: Outcome[], candidates: Outcome[]) : Outcome[] {
    if (candidates.length === 0) {
        return [];
    }
    else {
        const head = candidates[0];
        if (head.expected > getReserveRate(selected)) {
            const tail = candidates.slice(1);
            selected.push(head);
            return select(selected, tail);
        }
        else return selected;
    }
}

function getProportions (fractions: {[outcome: string]: number}, total: number) : {[outcome: string]: number} {
    let result: {[outcome: string]: number} = {};
    for (let outcome in fractions) {
        result[outcome] = fractions[outcome] / total;
    }
    return result;
}

function create (outcomes: Outcome[]) : Event {
    outcomes.sort((o1, o2) => o2.expected - o1.expected);
    let selection = select([], outcomes);
    let reserveRate = getReserveRate(selection);
    let fractions: {[outcome: string]: number} = {};
    let sumFractions: number = 0;
    outcomes.forEach (
        o => {
            let fraction = selection.includes(o) ? (o.prob - reserveRate / o.price) : 0;
            fractions[o.label] = fraction;
            sumFractions += fraction;
        }
    );
    let result: Event = {
        outcomes, selection, reserveRate, fractions, sumFractions,
        getProportions: total => getProportions(fractions, total)
    };
    console.log(result);
    return result;
}

function changeProb (source: Event, outcomeLabel: string, prob: number) : Event {
    let outcomes: Outcome[] = [];
    for (let outcome of source.outcomes) {
        if (outcome.label === outcomeLabel) {
            outcomes.push(Outcome.changeProb(outcome, prob));
        }
        else {
            outcomes.push(outcome);
        }
    }
    return create(outcomes);
}

function changeProbs (source: Event, probs: {[outcome: string]: number}) : Event {
    let outcomes: Outcome[] = [];
    for (let outcome of source.outcomes) {
        let prob: number | undefined = probs[outcome.label];
        if (prob === undefined) {
            outcomes.push(outcome);
        }
        else {
            outcomes.push(Outcome.changeProb(outcome, prob));
        }
    }
    return create(outcomes);
}